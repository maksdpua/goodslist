//
//  DetailScreen.swift
//  Goodslist
//
//  Created by iMax on 10/11/16.
//  Copyright © 2016 iMax. All rights reserved.
//

import UIKit

class DetailScreen: UIViewController, UITextViewDelegate, UITableViewDelegate, UITableViewDataSource
{
    
    @IBOutlet var textView: UITextView!
    
    @IBOutlet var buttonMinusView: UIView!
    @IBOutlet var buttonPlusView: UIView!
    
    @IBOutlet var minusCenterXconstraint: NSLayoutConstraint!
    @IBOutlet var minusCenterYconstraint: NSLayoutConstraint!
    
    @IBOutlet var plusCenterXconstraint: NSLayoutConstraint!
    @IBOutlet var plusCenterYconstraint: NSLayoutConstraint!
    
    @IBOutlet var closeCenterXconsttraint: NSLayoutConstraint!
    @IBOutlet var closeCenterYconstraint: NSLayoutConstraint!
    
    
    @IBOutlet var listTableView: UITableView!
    
    @IBOutlet var height: NSLayoutConstraint!
    
    let transitionManager = DetailTransitionManager()
    
    let databaseManager = Database()
    
    var list = [String]()
    
    var startHeight : CGFloat = 0.0
    var startMinusLeadingValue: CGFloat = 0.0
    var startPlusLeadingValue: CGFloat = 0.0
    
    fileprivate var presenting = true

    // MARK: - Lifecycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.transitioningDelegate = self.transitionManager
        
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        self.centerYupdates()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        UIApplication.shared.statusBarStyle = .default
    }
    
    // MARK: - Private methods
    private func centerYupdates()
    {
        UIView.animate(withDuration: 0.3, animations:
            {
                if self.presenting
                {
                    self.plusCenterXconstraint.constant = self.textView.frame.size.width/2 + self.height.constant/2 + 2
                    self.minusCenterXconstraint.constant = -self.textView.frame.size.width/2 - self.height.constant/2 - 2
                    self.closeCenterYconstraint.constant = self.height.constant + self.height.constant/2
                }
                else
                {
                    self.plusCenterXconstraint.constant = 0
                    self.minusCenterXconstraint.constant = 0
                    self.closeCenterYconstraint.constant = 0
                }
                
                self.view.layoutIfNeeded()
                
        }) { (finished) in
            if !self.presenting
            {
                self.dismiss(animated: true, completion: nil)
            }
            else
            {
                self.presenting = false
            }
        }
    }
    
    private func textViewAnimation()
    {
        CATransaction.begin()
        
        let groupAnimation = CAAnimationGroup()
        
        groupAnimation.duration = 0.5
        
        
        
        let scale = CABasicAnimation(keyPath: "transform")
        var tr = CATransform3DIdentity

        tr = CATransform3DScale(tr, 0.8, 0.8, 1)
        tr = CATransform3DScale(tr, 1.2, 1.2, 1)
        scale.fromValue = NSValue.init(caTransform3D: CATransform3DIdentity)

        scale.toValue = NSValue.init(caTransform3D: tr)
        
        groupAnimation.repeatCount = 2
        groupAnimation.speed = 3
        groupAnimation.animations = [scale]
        
        self.textView.layer.add(groupAnimation, forKey: "allAnimtaions")
        
        CATransaction.commit()
    }
    
    // MARK: - UITextView delegate methods
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView.text == "Needs some?"
        {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView.text == ""
        {
            textView.text = "Needs some?"
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n"
        {
            textView.resignFirstResponder()
            return false
        }
        
        if textView.text ==  "Needs some?"
        {
            textView.text = ""
        }
        
        return true
    }
    
    // MARK: TableView methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.classString(theClass: DetailListCell.self)) as! DetailListCell
        cell.label.text = list[indexPath.row]
        
        return cell
    }
    
    // MARK: - Actions
    @IBAction private func back(_ sender: AnyObject)
    {
        if list.count != 0
        {
            list.remove(at: 0)
            let indexPath = NSIndexPath.init(row: list.count, section: 0) as IndexPath
            listTableView.beginUpdates()
            listTableView.deleteRows(at: [indexPath], with: .automatic)
            listTableView.endUpdates()
            if list.count>0
            {
               listTableView.scrollToRow(at: NSIndexPath.init(row: list.count-1, section: 0) as IndexPath, at: .bottom, animated: true)
            }
        }
    }
    
    @IBAction private func plus(_ sender: AnyObject)
    {
        if textView.text == "Needs some?" || textView.text.isEmpty
        {
            self.textViewAnimation()
            return
        }
        
        list.insert(textView.text, at: list.count)

        let indexPath = NSIndexPath.init(row: list.count-1, section: 0) as IndexPath
        listTableView.beginUpdates()
        listTableView.insertRows(at: [indexPath], with: .automatic)
        listTableView.endUpdates()
        textView.text = "Needs some?"
        
        listTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
    }
    
    @IBAction func close(_ sender: AnyObject)
    {
        databaseManager.insertObjects(list)
        
        self.centerYupdates()
    }
    

}
