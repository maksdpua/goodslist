//
//  LaunchScreen.swift
//  Goodslist
//
//  Created by iMax on 10/8/16.
//  Copyright © 2016 iMax. All rights reserved.
//

import UIKit

class LaunchScreen: UIViewController, UIViewControllerTransitioningDelegate
{
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var centerY: NSLayoutConstraint!
    @IBOutlet var width: NSLayoutConstraint!
    @IBOutlet var height: NSLayoutConstraint!
    @IBOutlet var top: NSLayoutConstraint!

    // MARK: - Lifecycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.titleLabel.transform = CGAffineTransform(scaleX: 1.5, y:1.5)
    }

    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 0.25, delay: 1, options: .curveEaseInOut, animations: { 
            self.titleLabel.transform = CGAffineTransform.identity
        }) { (finished) in
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5)
                {
                    let listScreen = self.storyboard?.instantiateViewController(withIdentifier: self.classString(theClass: ListScreen.self))
                    
                    let transtition = MenuTransitionManager()
                    
                    
                    listScreen?.transitioningDelegate = transtition
                    
                    self.present(listScreen!, animated: true, completion: nil)
                }
        }
        
        
    }




}
