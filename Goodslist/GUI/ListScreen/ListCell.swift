//
//  ListCell.swift
//  Goodslist
//
//  Created by iMax on 10/8/16.
//  Copyright © 2016 iMax. All rights reserved.
//

import UIKit

class ListCell: UITableViewCell {
    
    @IBOutlet var nameLabel: UILabel?
    
    func prepare(name: String) -> ()
    {
        self.nameLabel?.text = name
    }

}
