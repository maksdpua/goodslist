//
//  ListScreen.swift
//  Goodslist
//
//  Created by iMax on 10/8/16.
//  Copyright © 2016 iMax. All rights reserved.
//

import UIKit

class ListScreen: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet var listTableView: UITableView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var lineView: UIView!
    @IBOutlet var buttonView: UIView!
    @IBOutlet var tabView: UIView!
    @IBOutlet var plusButton: UIButton!
    
    @IBOutlet var right: NSLayoutConstraint!
    @IBOutlet var bottom: NSLayoutConstraint!
    
    let databaseManager = Database()
    
    var startRight: CGFloat = 0.0
    var startBottom: CGFloat = 0.0
    
    fileprivate var presenting = true
    
    var tempArray = [ItemModel]()
    
    let cellSpacingHeight = 20
    
    // MARK: - Lifecycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func  viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        UserNotificationManager.shared.addNotificationWithAttachment(type: .image)
        
        if !self.presenting
        {
            UIView.animate(withDuration: 0.25, animations: { 
                self.right.constant = self.startRight
                self.bottom.constant = self.startBottom
                self.view.layoutIfNeeded()
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        let database = Database()
        tempArray = database.fetch()
        listTableView.reloadData()
    }
    
    // MARK - Actions
    
    @IBAction func back(_ sender: AnyObject)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func plus(_ sender: AnyObject)
    {
        UIView.animate(withDuration: 0.25, animations:
            {
                self.startRight = self.right.constant
                self.startBottom = self.bottom.constant
                
                self.right.constant = self.view.frame.size.width/2 - self.buttonView.frame.size.width/2
                self.bottom.constant = self.view.frame.size.height/2 - self.buttonView.frame.size.height/2
                self.view.layoutIfNeeded()
                
            }) { (finished) in
                
                
                let detailScreen = self.storyboard?.instantiateViewController(withIdentifier: self.classString(theClass: DetailScreen.self))
                
                
                self.present(detailScreen!, animated: true, completion: { 
                    self.presenting = false
                })
        }
        
    }
    
    
    // MARK: - UITableView methods
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return tempArray.count
    }
    
    func tableView(_ tableVie: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.classString(theClass: ListCell.self)) as! ListCell

        let item = tempArray[indexPath.section]
        
        if item.name != nil
        {
            cell.prepare(name: (item.name!))
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(cellSpacingHeight)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if (editingStyle == .delete)
        {
            tempArray = databaseManager.fetch()
            
            let object = tempArray[indexPath.section]
            
            databaseManager.deleteObject(object)
            
            tempArray.remove(at: indexPath.section)
            
            let indexSet = NSMutableIndexSet()
            indexSet.add(indexPath.section)
            
            listTableView.beginUpdates()
            
            listTableView.deleteSections(indexSet as IndexSet, with: .automatic)
            
            listTableView.endUpdates()
        }
    }
    
    
    
}
