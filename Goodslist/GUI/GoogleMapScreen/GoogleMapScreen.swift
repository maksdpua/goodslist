//
//  GoogleMapScreen.swift
//  Goodslist
//
//  Created by Max Shvets on 2/23/17.
//  Copyright © 2017 iMax. All rights reserved.
//

import UIKit
import GoogleMaps

class GoogleMapScreen: UIViewController
{
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var adressLabel: UILabel!
    
    let locationManager = CLLocationManager()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        mapView.delegate = self
    }
    
    func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D)
    {
        
        // 1
        let geocoder = GMSGeocoder()
        
        // 2
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            if let address = response?.firstResult() {
                
                // 3
                let lines = address.lines! as [String]
                self.adressLabel.text = lines.joined(separator: "\n")
                
                let labelHeight = self.adressLabel.intrinsicContentSize.height
                self.mapView.padding = UIEdgeInsets(top: self.topLayoutGuide.length, left: 0,
                                                    bottom: labelHeight, right: 0)
                // 4
                self.updateViewAnimated()
            }
        }
    }
    
    func updateViewAnimated()
    {
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }

    @IBAction func back(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
}


// MARK: - CLLocationManagerDelegate extension
extension GoogleMapScreen: CLLocationManagerDelegate {
    // 2
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            
            // 4
            locationManager.startUpdatingLocation()
            
            //5
            mapView.isMyLocationEnabled = true
            mapView.settings.myLocationButton = true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            
            // 7
            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            
            // 8
            locationManager.stopUpdatingLocation()
        }
    }
    
    // 6
}

// MARK: - GMSMapViewDelegate extension
extension GoogleMapScreen: GMSMapViewDelegate
{
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition)
    {
        reverseGeocodeCoordinate(coordinate: position.target)
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool)
    {
        self.updateViewAnimated()
    }
}
