//
//  ItemModel+CoreDataProperties.swift
//  
//
//  Created by iMax on 10/15/16.
//
//

import Foundation
import CoreData


extension ItemModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ItemModel> {
        return NSFetchRequest<ItemModel>(entityName: "ItemModel");
    }

    @NSManaged public var name: String?
    @NSManaged public var date: Int16
    @NSManaged public var photo: String?

}
