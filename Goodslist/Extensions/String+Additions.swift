//
//  String+Additions.swift
//  Goodslist
//
//  Created by iMax on 10/9/16.
//  Copyright © 2016 iMax. All rights reserved.
//

import UIKit

extension NSObject
{
    public func classString(theClass: AnyClass) -> String
    {
        return String(describing: theClass)
    }
}
