//
//  NSDate+Additions.swift
//  Goodslist
//
//  Created by Maks on 10/18/16.
//  Copyright © 2016 iMax. All rights reserved.
//

import UIKit

let CURRENT_DAY_DATE_FORMATTE = "HH:mm"
let DAY_BEFORE_DATE_FORMATTE = "dd MMMM"
let BEFORE_YEAR_DATE_FORMATTE = "dd MMMM yyyy"

class DateModifier: NSObject
{
    func currentDate() -> ()
    {
        // *** Create date ***
        let date = NSDate()
        
        // *** create calendar object ***
        var calendar = NSCalendar.current
        
        // *** Get components using current Local & Timezone ***
        print(calendar.dateComponents([.year, .month, .day, .hour, .minute], from: date as Date))
        
        // *** define calendar components to use as well Timezone to UTC ***
        let unitFlags = Set<Calendar.Component>([.hour, .year, .minute])
        calendar.timeZone = TimeZone(identifier: "UTC")!
        
        // *** Get components from date ***
        let components = calendar.dateComponents(unitFlags, from: date as Date)
        print("Components : \(components)")
    }
    
    func currentDateString() -> String
    {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd MMMM yyyy"
        let currentDate = NSDate()
        
        return dateFormatter.string(from: currentDate as Date)
    }
}

extension NSDate
{
//    func date(_ hour: NSInteger, min: NSInteger, daysModifier: NSInteger) -> NSDate
//    {
//        let calendar = NSCalendar.current
//        let components = NSDate().components()
//        components.day = components.day + daysModifier
//        components.hour = hour
//        components.minute = min
//        components.second = 0
//        
//        return components.date
//    }
//    
//    func todayDateWithHour(_ hour: NSInteger,  min: NSInteger) -> NSDate
//    {
//        return NSDate()
//    }
//    
//    func dateStringByTime(_ time: NSInteger) -> String
//    {
////        let dateByTime = NSDate.init(timeIntervalSince1970: time)
////        let today =
//        
//        return ""
//    }
//    
//    func components() -> NSDateComponents
//    {
//        let unitFlags = Set<Calendar.Component>([.hour, .year, .minute])
//        let calendar = Calendar.current
//        let components =  calendar.dateComponents(unitFlags, from: self as Date)
//        return components as NSDateComponents
//
//    }
}

