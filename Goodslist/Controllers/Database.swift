//
//  Database.swift
//  Goodslist
//
//  Created by iMax on 10/15/16.
//  Copyright © 2016 iMax. All rights reserved.
//

import CoreData

class Database: NSObject, NSFetchedResultsControllerDelegate
{
    
    lazy var applicationDocumentsDirectory: NSURL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1] as NSURL
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: "GoodsData", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Publec Methods
    func insertNewObject(_ name: String)
    {
        let entityDes = NSEntityDescription.entity(forEntityName: "ItemModel", in: managedObjectContext)
        
        let entity = ItemModel(entity: entityDes!, insertInto: managedObjectContext)

        entity.name = name
        
        do {
            try managedObjectContext.save()
        } catch {
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
    }
    
    func insertObjects(_ array:[String])
    {
        for name in array
        {
            self.insertNewObject(name)
        }
    }
    
    func fetch() -> [ItemModel]
    {
        let itemFetch = NSFetchRequest<ItemModel>(entityName: "ItemModel")
        
        let fetchedPerson : [ItemModel]
        
        do {
            fetchedPerson = try managedObjectContext.fetch(itemFetch)
            
        } catch {
            fatalError("Failed to fetch person: \(error)")
        }

        return fetchedPerson
    }
    
    func deleteObject(_ object: ItemModel)
    {
        managedObjectContext.delete(object)
        
        self.saveContext()
    }
    
    func saveContext ()
    {
        if managedObjectContext.hasChanges
        {
            do {
                try managedObjectContext.save()
            } catch {
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
}

