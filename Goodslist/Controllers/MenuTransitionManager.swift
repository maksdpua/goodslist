//
//  MenuTransitionManager.swift
//  Goodslist
//
//  Created by iMax on 10/9/16.
//  Copyright © 2016 iMax. All rights reserved.
//

import UIKit

class MenuTransitionManager: NSObject, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate
{
    
    // MARK: - UIViewControllerAnimatedTransitioning delegate methods
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning)
    {
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to) as! ListScreen
        let finalFrameForVC = transitionContext.finalFrame(for: toViewController)
        let containerView = transitionContext.containerView
        
        toViewController.view.frame = finalFrameForVC
        
        toViewController.titleLabel.transform = CGAffineTransform(translationX: 0, y: toViewController.view.center.y)
        toViewController.listTableView.transform = CGAffineTransform(translationX: 0, y: toViewController.view.frame.size.height)
        toViewController.buttonView.transform = CGAffineTransform(translationX: 0, y: -toViewController.view.frame.size.height - toViewController.buttonView.frame.size.height)
        toViewController.tabView.transform = CGAffineTransform(translationX: toViewController.tabView.frame.size.width, y: 0)
        
        
        let lineRect = toViewController.lineView.frame
        
        toViewController.lineView.frame = CGRect(x: 0, y: lineRect.origin.y, width: 0, height: lineRect.size.height)
        
        containerView.addSubview(toViewController.view)
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.8, options: .curveEaseOut, animations:
            {
                toViewController.titleLabel.transform = CGAffineTransform.identity
        }) { (finished) in

        }
        
        UIView.animate(withDuration: 1.2, delay: 1, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.8, options: .curveEaseOut, animations:
            {
                toViewController.listTableView.transform = CGAffineTransform.identity
        }) { (finished) in
            
        }
        
        UIView.animate(withDuration: 1.2, delay: 1.5, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.8, options: .curveEaseOut, animations:
            {
                toViewController.lineView.frame = lineRect
                toViewController.buttonView.transform = CGAffineTransform.identity
                toViewController.tabView.transform = CGAffineTransform.identity
                
        }) { (finished) in
            transitionContext.completeTransition(true)
        }
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval
    {
        return 1
    }
    
    // MARK: - UIViewControllerTransitioningDelegate
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        return self
    }
    
    // return the animator used when dismissing from a viewcontroller
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        return self
    }
    
}
