//
//  UserNotificationManager.swift
//  Goodslist
//
//  Created by iMax on 10/17/16.
//  Copyright © 2016 iMax. All rights reserved.
//

import UIKit
import UserNotifications
import CoreLocation

enum AttachmentType
{
    case image
    case imageGif
    case audio
    case video
}

class UserNotificationManager: NSObject {
    
    static let shared = UserNotificationManager()
    
    override init() {
        super.init()
        UNUserNotificationCenter.current().delegate = self
    }
    
    func registerNotification()
    {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
            
        }
    }
    
    // MARK: - Add Default Notification
    func addNotificationWithTimeIntervalTrigger()
    {
        let content = UNMutableNotificationContent()
        content.title = "Title"
        content.subtitle = "Subtitle"
        content.body = "Body"
        content.badge = 1
        content.sound = UNNotificationSound.default()
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 3, repeats: false)
        
        let  request = UNNotificationRequest(identifier: "TimeInterval", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request) { (error) in
            
        }
    }
    
    func addCalendarNotificationTrigger()
    {
        let content = UNMutableNotificationContent()
        content.title = "Title"
        content.subtitle = "Subtitle"
        content.body = "Body"
//        content.badge = 1
        content.sound = UNNotificationSound.default()
        
        var components = DateComponents()
//        components.weekday = 1
        components.hour = 2
        components.minute = 33
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: true)
        
        let  request = UNNotificationRequest(identifier: "Calendar", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request) { (error) in
            
        }
    }
    
    func addLocactionTrigger()
    {
        let content = UNMutableNotificationContent()
        content.title = "Title"
        content.subtitle = "Subtitle"
        content.body = "Body"
        //        content.badge = 1
        content.sound = UNNotificationSound.default()
        
        let center = CLLocationCoordinate2DMake(48.45, 34.983333)
        
        let region = CLCircularRegion(center: center, radius: 10000, identifier: "Dnepr")
        
        
        region.notifyOnEntry = true
        region.notifyOnExit = true
        
        let trigger = UNLocationNotificationTrigger(region: region, repeats: true)
        
        let  request = UNNotificationRequest(identifier: "Location", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request) { (error) in
            
        }
    }
    
    func addContent(_ subtitle: String) -> UNMutableNotificationContent
    {
        let content = UNMutableNotificationContent()
        content.title = "Title"
        content.subtitle = "Subtitle"
        content.body = "Body"
        content.badge = 1
        content.sound = UNNotificationSound.default()
        
        return content
    }
    
    func addNotificationWithDelay(_ content: UNNotificationContent) -> ()
    {
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 3, repeats: false)
        
        let  request = UNNotificationRequest(identifier: "TimeInterval", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request) { (error) in
            print("error")
        }
    }
    
   // MARK: - Add Notification with attachment
    func addNotificationWithAttachment(type: AttachmentType)
    {
        let contentSubtitle = ""
        var url: URL?
        
        switch type {
        case .image:
            url = Bundle.main.url(forResource: "smile", withExtension: "jpg")
            break
        default:
            break
        }
        
        let content = addContent(contentSubtitle)
        
        do {
            let arrachment = try UNNotificationAttachment(identifier: "arrach", url: url!, options: nil)
            content.attachments = [arrachment]
        } catch {
           print("Attachment error")
        }
        
        self.addNotificationWithDelay(content)
    }

}

extension UserNotificationManager: UNUserNotificationCenterDelegate
{
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        print("willPresentNotification")
        completionHandler([.alert, .sound])
    }
}
