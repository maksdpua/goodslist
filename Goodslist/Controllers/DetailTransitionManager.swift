//
//  DetailTransitionManager.swift
//  Goodslist
//
//  Created by iMax on 10/11/16.
//  Copyright © 2016 iMax. All rights reserved.
//

import UIKit

class DetailTransitionManager: NSObject, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate, CAAnimationDelegate
{
    fileprivate var presenting = false
    
   // MARK: - UIViewControllerAnimatedTransitioning delegate methods
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning)
    {
        let screens : (from:UIViewController, to:UIViewController) = (transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!, transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!)
        
        let secondVC = !self.presenting ? screens.from as! DetailScreen : screens.to as! DetailScreen
        let firstVC = !self.presenting ? screens.to as! ListScreen : screens.from as! ListScreen
        
        let containerView = transitionContext.containerView
        
        containerView.addSubview(firstVC.view)
        containerView.addSubview(secondVC.view)
        
        CATransaction.begin()
        
        CATransaction.setCompletionBlock
        {
             transitionContext.completeTransition(true)
        }
        
        if (self.presenting)
        {
            let scale = CABasicAnimation(keyPath: "transform")
            
            var tr = CATransform3DIdentity
            tr = CATransform3DScale(tr, 0.1, 0.1, 1)
            
            scale.fromValue = NSValue.init(caTransform3D: tr)
            scale.toValue = NSValue.init(caTransform3D: CATransform3DIdentity)
            
            let corneredAnimation = CABasicAnimation(keyPath: "cornerRadius")
            let cornerR = firstVC.view.frame.size.height/2
            
            corneredAnimation.fromValue = cornerR
            corneredAnimation.toValue = NSNumber.init(value: 0)
            
            let group =  CAAnimationGroup()
            
            group.duration = 0.3
            
            group.animations = [scale, corneredAnimation]
            
            secondVC.view.layer.add(group, forKey: "allMyAnimations")
        }
        else
        {
            let scale = CABasicAnimation(keyPath: "transform")
            var tr = CATransform3DIdentity
            tr = CATransform3DScale(tr, 0.1, 0.1, 1)
            scale.toValue = NSValue.init(caTransform3D: tr)
            
            let corneredAnimation = CABasicAnimation(keyPath: "cornerRadius")
            let cornerR = secondVC.view.frame.size.height/2
            
            corneredAnimation.fromValue = NSNumber.init(value: 0)
            corneredAnimation.toValue = cornerR
            
            let group =  CAAnimationGroup()
            group.duration = 0.25
            group.animations = [scale, corneredAnimation]
            group.isRemovedOnCompletion = false
            group.fillMode = kCAFillModeForwards
            
            secondVC.view.layer.add(group, forKey: "allMyAnimations")
        }
        CATransaction.commit()
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval
    {
        return 1
    }
    
    // MARK: - UIViewControllerTransitioningDelegate
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        self.presenting = true
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        self.presenting = false
        return self
    }
}
